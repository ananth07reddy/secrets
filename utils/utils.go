package utils

// TODO: rename to a meaningful name

import (
	log "github.com/sirupsen/logrus"
)

// WithWarning runs the function passed as argument and prints a warning if it returns an error
func WithWarning(warning string, fun func() error) {
	err := fun()
	if err != nil {
		log.Warnf("Warning: %s (%s)\n", warning, err.Error())
	}
}
